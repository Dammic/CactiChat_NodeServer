/**
 * Requires
 */
var express = require('express');
var app = express();
var morgan = require('morgan');
var bodyParser = require('body-parser');
var path = require('path');
var config = require('./config.js');

/**
 * Routes
 */
var user = require("./routes/user.js");
var image = require("./routes/image.js");
var auth = require("./routes/auth.js");


app.set('superSecret', config.secret);
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: false, limit: '50mb'}));

app.use("/auth", auth);
app.use("/user", user);
app.use("/image", image);


app.use(express.static(path.join(__dirname, './public')));
app.use(express.static(__dirname + '/public/resources'));
app.use(express.static(__dirname + '/public/gallery'));
app.use(morgan('dev'));


// listen (starting server)
var port = process.env.PORT || 5000;
app.listen(port);
console.log("Server listening on https://cactichat-server.herokuapp.com:"+port+"...");
