var express = require('express');
var router = express.Router();
var path = require('path');
var mongo = require('../handlers/dbHandler.js');
var ObjectId = mongo.ObjectId;
var clc = require('cli-color');
var check = require('check-types');
var error = clc.red.bold;
var warn = clc.yellow;
var notice = clc.blue.bold;
var _ = require("lodash");
var verifyToken = require("../handlers/tokenHandler.js").verifyToken;




/**
 * @params: (Array) recipientsList
 *          (String64) imageString
 *     It expects at least two parameters - recipientsList and imageString. You should pass them in request
 * @functionality: This route sends an image to another user
 *        It first checks if the required parameters are present,
 *        Then it checks if the sender exists
 *        Then it just inserts new image to the image collection
 * @statusCodes:
 *      "error" - in case of problem with connection to database, internal problem or invalid parameters
 *      "ok" -    the image was successfully sent
 * @todo: testing
 * @todo: discussion - should those images really be outside of users collection? (privacy issues)
 * @todo: is senderName necessary? (and how to get rid of it)
 * @todo: is there something 'slightly' better than base64 for sending images? It's soo huge...
 */
router.post('/sendImage', function (req, res, next) {
    verifyToken(req.body.token, (err, decodedToken) =>{
        if(err){
            console.log(error("Error - the token is either not present or invalid! Stopping request..."));
            res.status(403).send({"status" : "unauthorized"}).end();
            return;
        }
        console.log(notice("An user wants to send image, initiating..."));
        console.log(notice("Parsing passed arguments..."));
        var userID = ObjectId(decodedToken._id);
        var recipientsList = req.body.recipientsList;
        var recipientsObjArray = [];
        _.forEach(recipientsList, (value) => {
            if(!(value.match(/^[0-9a-fA-F]{24}$/)))  recipientsList.pop(value);
            else recipientsObjArray.push(ObjectId(value));
        });
        recipientsList = recipientsObjArray;
        var imageString = req.body.imageString;
        if(check.not.array(recipientsList)){
            console.log(error("Failed to send image - the expected parameters were not present or invalid!"));
            res.status(200).send({"status" : "error"}).end();
        }else {
            console.log(notice("Establishing connection with database..."));
            mongo.connect("serverdb", ["user", "image"], function (db) {
                console.log(notice("Checking if the sender exists..."));
                db.user.findOne({"_id": userID},{"login" : 1}, (err, data) => {
                    if (err) {
                        console.log(error("Failed to send image - there was an error while connecting to the database!"));
                        res.status(200).send({"status": "error"}).end(() => {
                            db.close();
                        });
                    } else {
                        if (data.length == 0) {
                            console.log(error("Failed to send image - the sender does not exists!"));
                            res.status(200).send({"status": "error"}).end(() => {
                                db.close();
                            });
                        } else {
                            var doc = {
                                "senderID" : userID,
                                "senderName": data.login,
                                "recipientsList": recipientsList,
                                "sentDate": new Date(),
                                "isExtendedView": false,
                                "viewLength": 10,
                                "imageString": imageString
                            };


                            db.image.insert(doc, (err, data) => {
                                if (err) {
                                    console.log(error("Failed to send image - there was an error while connecting to the database!"));
                                    res.status(200).send({"status": "error"}).end(() => {
                                        db.close();
                                    });
                                } else {
                                    console.log(notice("Image successfully sent!"));
                                    res.status(200).send({"status": "ok"}).end(() => {
                                        db.close();
                                    });
                                }
                            });
                        }
                    }
                });
            });
        }
    });

});





/**
 * @params: (String) imageIDs
 *     It expects at least one parameter - imageID. You should pass it in request
 * @functionality: This route gets detail of image with id <imageID>
 *        It first checks if the required parameters are present,
 *        Then it just gets image's detail
 *        But before it returns with data, it removes the image from database.
 * @statusCodes:
 *      "error" - in case of problem with connection to database, internal problem or invalid parameters
 *      "ok" -    the image data was successfully obtained
 * @todo: testing
 */
router.post('/getImageDetail', function (req, res, next) {
    verifyToken(req.body.token, (err, decodedToken) => {
        if (err) {
            console.log(error("Error - the token is either not present or invalid! Stopping request..."));
            res.status(403).send({"status": "unauthorized"}).end();
            return;
        }
        console.log(notice("An user wants to get image detail, initiating..."));
        console.log(notice("Parsing passed arguments..."));
        var userID = ObjectId(decodedToken._id);
        var imageID = req.body.imageID;
        if (!(imageID.match(/^[0-9a-fA-F]{24}$/))) {
            console.log(error("Failed to get image detail - the expected parameters were not present or invalid!"));
            res.status(200).send({"status": "error"}).end();
        } else {
            imageID = ObjectId(imageID);

            console.log(notice("Establishing connection with database..."));
            mongo.connect("serverdb", ["image", "user"], function (db) {
                db.image.find({"_id": imageID}, (err, data) => {
                    if (err) {
                        console.log(error("Failed to get image detail - there was an error while connecting to the database!"));
                        res.status(200).send({"status": "error"}).end(() => {
                            db.close();
                        });
                    } else {
                        console.log(notice("Successfully obtained image detail!"));
                        deleteUserFromImageRecipients(userID, imageID, () => {
                            res.status(200).send({"status": "ok", "data": data[0]}).end(() => {
                                db.close();
                            });
                        });
                    }
                })
            });
        }
    });
});



/**
 * @params: (String/ObjectId) userID
 *          (String/ObjectId) imageID
 *          (function) callback
 *     It expects at least two parameters - userID and imageID. You should pass them in request
 * @functionality: This function pulls the specific user out of specific image's recipient list
 * @statusCodes:
 *      "error" - in case of problem with connection to database, internal problem or invalid parameters
 *      "ok" -    the user was successfully pulled out of image's recipientsList
 * @todo: testing
 */
function deleteUserFromImageRecipients(userID, imageID, callback){
    console.log(notice("Deleting user "+userID+" from recipients of image "+imageID));
    imageID = ObjectId(imageID);
    userID = ObjectId(userID);

    console.log(notice("Establishing connection with database..."));
    mongo.connect("serverdb", ["image"], function (db) {
        db.image.update({"_id": imageID},{$pull :{"recipientsList" : userID}}, (err, data) => {
            if (err) {
                console.log(error("Failed to remove user from recipients - there was an error while connecting to the database!"));
                db.close();
                if(callback)  callback({"status" : "error"});
            } else {
                console.log(notice("Successfully removed user "+userID+ " from recipients of image with id "+ imageID+"!"));
                db.close();
                if(callback)  callback({"status" : "ok"});
            }
        })
    });
}



module.exports = router;