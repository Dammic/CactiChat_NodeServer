var express = require('express');
var router = express.Router();
var path = require('path');
var mongo = require('../handlers/dbHandler.js');
var ObjectId = mongo.ObjectId;
var clc = require('cli-color');
var check = require('check-types');
var error = clc.red.bold;
var warn = clc.yellow;
var notice = clc.blue.bold;
var crypto = require('crypto');
var bcrypt = require('bcrypt-nodejs');
var secret = require('../config.js').secret;
var jwt = require('jsonwebtoken');
var ms = require('ms');
var verifyToken = require("../handlers/tokenHandler.js").verifyToken;






/**
 * @params: (String) login
 *          (String) password
 *          It expects at least two parameters - login and password. You should pass them in request
 * @functionality: This route registers a new user.
 *      It first checks if the required parameters are present,
 *      then it checks if there is any user with the same login in a database.
 *          In case it finds somebody, it returns {"status" : "taken"}
 *          If it does not, it inserts a new document to the database (see doc variable below)
 *              and sends {"status" : "ok"} back to the client
 *          But the most important thing is - it generates a random salt for the password
 *              appends it at the beginning of the passed password and then
 *              hashes it with bcrypt algorithm
 *              then it stores the hash in the database, as well as the salt used for hashing
 *          Otherwise, in case of server error or inaccesibility, it returns {"status" : "error"}
 *              (there are two contacts with db, so it can return it in two places)
 * @statusCodes:
 *      "error" - in case of problem with connection to database, internal problem or invalid parameters
 *      "taken" - in case the user already exists in the database
 *      "ok" -    the user has been successfully registered
 * @todo: registration should also login the user as well.
 * @todo: testing
 */
router.post('/register', function (req, res, next) {
    console.log(notice("A new user wants to register, initiating..."));
    console.log(notice("Parsing passed arguments..."));
    var login = req.body.login;
    var password = req.body.password;
    if(!login || !password ){
        console.log(error("Failed to register user - the expected parameters were not present or invalid!"));
        res.status(200).send({"status" : "error"}).end();
    }else {

        var salt = crypto.randomBytes(20).toString('hex');
        password = salt+password;
        var doc = {
            "login": login,
            "password": bcrypt.hashSync(password),
            "passwordSalt": salt,
            "friendsList": [],
            "friendRequestsList": [],
            "settings": {}
        };
        console.log(notice("Arguments passed successfully! Received data: login: " + login + ", password: secret"));
        console.log(notice("Establishing connection with database..."));
        mongo.connect("serverdb", ["user"], function (db) {
            db.user.find({"login": login}, (err, data) => {
                if (err) {
                    console.log(error("Failed to register user - there was an error while connecting to the database!"));
                    res.status(200).send({"status": "error"}).end(() => {
                        db.close();
                    });
                } else {
                    if (data.length != 0) {
                        console.log(warn(login + " wanted to register, but the credentials were already taken!"));
                        res.status(200).send({"status": "taken"}).end(() => {
                            db.close();
                        });
                    } else {
                        console.log(notice("There was nobody with username " + login + ", registering new user..."));
                        db.user.insert(doc, (err, data) => {
                            if (err) {
                                res.status(200).send().end(() => {
                                    console.log(error("Failed to register user - there was an error while connecting to the database!"));
                                    db.close({"status": "error"});
                                });
                            } else {
                                console.log(notice("User successfully registered! Logging the user..."));
                                var token = jwt.sign({"_id": data._id}, secret, {
                                    "expiresIn": ms('1d')   // expires in 1 day
                                });
                                res.status(200).send({"status": "ok", "token" : token}).end(() => {
                                    db.close();
                                });
                            }
                        });
                    }
                }
            })
        });
    }
});





/**
 * @params: (String) login
 *          (String) password
 *          It expects at least two parameters - login and password. You should pass them in request
 * @functionality: This route logins an user
 *      It first checks if the required parameters are present,
 *      Then it checks if there is any somebody with the same login, then fetches it's password and passwordSalt
 *          Then it hashes the passed password with salt and compares it to the stored password hash in the db
 *          In case of successful comparison, it
 *          generated a new token for the user (consisting of user's id),
 *          then it returns {"status" : "ok", "token" : token}
 *          If it does not, it returns {"status" : "unauthorized"}
 *          Otherwise, in case of server error or inaccesibility, it returns {"status" : "error"}
 * @statusCodes:
 *      "error" - in case of problem with connection to database, internal problem or invalid parameters
 *      "unauthorized" - in case the user provided incorrect credentials
 *      "ok" -    the user has been successfully logged in and confirmed that he is himself
 * @todo: we should have tokens. currently, the login phase is just to skip the login screen
 *        but we cannot trust in the good intentions of the client. So it should look kind of like:
 *        if the user logs in and does not have a token, we do a standard checkup in the db, and then
 *        generate the token for him based on his login and hash of his password (?) and return it to him,
 *        as well as store it somewhere on the server. If the user has the token, we should authorize him immediately
 *        Tokens should be connected with logins, so we can identify the users by their tokens.
 *        Tokens should have an expiry time of 6 hours.
 *        Each functionality in the db should have checking of the token.
 * @todo: testing
 * @todo: SSL and https to protect from repetition attacks/mitm
 * @todo: check logging with some utf-8 characters (20 utf-8 characters) - they may exceed the limit and cause compare to always return true
 */
router.post('/login', function (req, res, next) {
    console.log(notice("An user wants to login, initiating..."));
    console.log(notice("Parsing passed arguments..."));
    var login = req.body.login;
    var password = req.body.password;
    if(!login || !password ){
        console.log(error("Failed to login user - the expected parameters were not present or invalid!"));
        res.status(200).send({"status" : "error"}).end();
    }else {

        console.log(notice("Arguments passed successfully! Received data: login: " + login + ", password: secret"));
        console.log(notice("Establishing connection with database..."));
        mongo.connect("serverdb", ["user"], function (db) {
            db.user.findOne({"login": login}, {"login" : 1, "password" : 1, "passwordSalt" : 1}, (err, data) => {
                if (err) {
                    console.log(error("Failed to login user - there was an error while connecting to the database!"));
                    res.status(200).send({"status": "error"}).end(() => {
                        db.close();
                    });
                } else {
                    //even if we don't find an user in db, it still needs to take the same amount of time to process it
                    //if it doesn't, the hacker will know an user does not exist (which is not what we want)
                    //processing of unknown user: 5 sec, processing of known user: 3 sec  @todo: some kind of wait()?? or other fake credentials?

                    if (data == null){
                        data = {};
                        data.passwordSalt  =  '000000000000000';
                        data.password      =  bcrypt.hashSync('000000000000000');
                        password           =  '111111111111111';

                    }


                    console.log(notice("Checking passed credentials..."));
                    password = data.passwordSalt+password;

                    bcrypt.compare(password, data.password, (err, result) => {
                        if (err || !result) {
                            console.log(warn("Login failed - user " + login + " wanted to log in, but the credentials were incorrect!"));
                            res.status(200).send({"status": "unauthorized"}).end(() => {
                                db.close();
                            });
                        } else {
                            console.log(notice("User successfully logged in! Generating token..."));
                            var token = jwt.sign({"_id": data._id}, secret, {
                                "expiresIn": ms('1d')   // expires in 1 day
                            });

                            console.log(notice("Token successfully generated! Sending response..."));
                            res.status(200).send({"status": "ok", "token": token}).end(() => {
                                db.close();
                            });
                        }
                    });

                }
            })
        });
    }
});






/**
 * @params: (String) token
 *          It expects at least one parameter - token. You should pass it in request
 * @functionality: This route logins an user via token
 *      It first verifies the authenticity of the token. If it's valid, the login was successful, if not, it was not.
 *      Then it sends the response to the client
 * @statusCodes:
 *      "unauthorized" - in case the user provided incorrect token
 *      "ok" -    the user has been successfully logged in and confirmed that he is himself
 * @todo: testing
 */
router.post('/loginByToken', function (req, res, next) {
    verifyToken(req.body.token, (err, decodedToken) => {
        if(err){
            console.log(warn("Automatic login failed - user did not provide a token or it was invalid!"));
            res.status(200).send({"status": "unauthorized"}).end(() => {
                db.close();
            });
        }else{
            console.log(warn("Login successful! The provided token is valid."));
            res.status(200).send({"status": "ok"}).end(() => {
                db.close();
            });
        }
    });
});


module.exports = router;


