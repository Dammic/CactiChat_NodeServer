var express = require('express');
var router = express.Router();
var path = require('path');
var mongo = require('../handlers/dbHandler.js');
var ObjectId = mongo.ObjectId;
var clc = require('cli-color');
var check = require('check-types');
var error = clc.red.bold;
var warn = clc.yellow;
var notice = clc.blue.bold;
var _ = require('lodash');
var verifyToken = require("../handlers/tokenHandler.js").verifyToken;



//@todo for all: surround converting to ObjectId with try catches (for node, of course) (and - do we need it now?)


/**
 * @params: None
 * @functionality: This route obtains information about user's friendsList, friendRequestsList and imagesArray
 *        It first checks for the user with the passed login and gets it's friendsList and friendRequestsList
 *              db has only information about friends ID's, so we need to lookup every id and get it's login
 *              so now every friend will be like {"_id" : id, "userName" : login}
 *          Then it connects to image collections and obtains all images, where passed login is inside recipientsList
 *          and gets the sentDate of those images, as well as senderName and _id. The images are then appended into
 *          'data' object as 'imagesArray', and returned in the same jsondocument as {"status" : "ok"}
 *          (So the returned data is {"status" : "ok", "data" : data}
 *        Otherwise, in case of server error or inaccesibility, it returns {"status" : "error"}
 * @statusCodes:
 *      "error" - in case of problem with connection to database, internal problem or invalid parameters
 *      "ok" -    the refresh has been successfully completed (all queried data has been gathered)
 * @todo: testing
 * @todo: prevent the user from mashing the refresh button (in application)
 */
router.post('/refresh', function (req, res, next) {
    verifyToken(req.body.token, (err, decodedToken) => {
        if (err) {
            console.log(error("Error - the token is either not present or invalid! Stopping request..."));
            res.status(403).send({"status": "unauthorized"}).end();
            return;
        }

        console.log(notice("An user wants to refresh, initiating..."));
        console.log(notice("Parsing passed arguments..."));
        var userID = ObjectId(decodedToken._id);
        mongo.connect("serverdb", ["user"], function (db) {
            db.user.findOne({"_id": userID},
            {'_id': 0, 'friendsList': 1, 'friendRequestsList': 1}, (err, userData) => {
                if (err) {
                    console.log(error("Failed to refresh - there was an error while connecting to the database!"));
                    res.status(200).send({"status": "error"}).end(() => {
                        db.close();
                    });
                } else {
                    console.log(notice("Fetching friendsList..."));
                    db.user.find({"_id": {$in: userData.friendsList}}, {"login": 1}, (err, data) => {
                        if (err) {
                            console.log(error("Failed to refresh - there was an error while connecting to the database!"));
                            res.status(200).send({"status": "error"}).end(() => {
                                db.close();
                            });
                        } else {
                            console.log(notice("FriendList fetched successfully!"));
                            userData.friendsList = [];
                            _.forEach(data, (value, key) => {
                                userData.friendsList.push({"userName": value.login, "_id": value._id})
                            });
                            console.log(notice("Fetching friendRequestsList..."));
                            db.user.find({"_id": {$in: userData.friendRequestsList}}, {"login": 1}, (err, data) => {
                                if (err) {
                                    console.log(error("Failed to refresh - there was an error while connecting to the database!"));
                                    res.status(200).send({"status": "error"}).end(() => {
                                        db.close();
                                    });
                                } else {
                                    console.log(notice("FriendRequestsList fetched successfully!"));
                                    userData.friendRequestsList = [];
                                    _.forEach(data, (value, key) => {
                                        userData.friendRequestsList.push({
                                            "userName": value.login,
                                            "_id": value._id
                                        })
                                    });
                                    mongo.connect("serverdb", ["image"], function (db) {
                                        console.log(userID);
                                        db.image.find({"recipientsList": userID},
                                            {'sentDate': 1, 'senderName': 1, 'senderID': 1}, (err, imagesArray) => {
                                                if (err) {
                                                    console.log(error("Failed to refresh - there was an error while connecting to the database!"));
                                                    res.status(200).send({"status": "error"}).end(() => {
                                                        db.close();
                                                    });
                                                } else {
                                                    userData.imagesArray = imagesArray;

                                                    console.log(notice("Successfully obtained data for refresh!"));
                                                    res.status(200).send({
                                                        "status": "ok",
                                                        'data': userData
                                                    }).end(() => {
                                                        db.close();
                                                    });
                                                }
                                            }
                                        );
                                    });
                                }
                            });
                        }
                    });
                }
            })
        });
    });
});





/**
 * @params: (String) friendID
 *          It expects at least one parameter - friendID. You should pass it in request
 *          Explanation:
 *                  <userID> confirms that he knows <friendID> and wants to include him in his friendList
 * @functionality: This route adds a friendship between userID and friendID
 *                 It does that by adding each other to each other's friendList
 *        It first checks if the required parameters are present and valid,
 *        Then it checks if the userID is not the same as friendID (if it is, returns {"status" : "himself"}
 *        Then it checks if the bond exists already between users (it tries to locate those users, that are:
 *              - logins one of the following: [userID, friendID]
 *              - their friendList-s contain one of the following: [userID, friendID]
 *              So all routes:
 *                  * userID contains userID inside his friendList (he is his own friend - unacceptable)
 *                  * userID contains friendID inside his friendList (he already has this friend - unacceptable)
 *                  * friendID contains friendID on his friendList - same as 1st
 *                  * friendID contains userID on his friendList - same as 2nd
 *              Will be rejected
 *        Then it adds a friendship to each user
 *        Otherwise, in case of server error or inaccesibility, it returns {"status" : "error"}
 * @statusCodes:
 *      "error" - in case of problem with connection to database, internal problem or invalid parameters
 *      "himself" - in case the user wants to add himself as a friend
 *      "already" - in case the user already has a friendship bond with the passed friendName
 *      "ok" -    the friendship bond was successfully created between the passed users
 * @warning: if some kind of failure will happen after inserting one friend and the second query
 *              could not be completed, we will end up with inconsistent data!
 *              we would have to implement our own rollback mechanism for that
 * @todo: testing
 * @todo: removing from friendRequestList after successfully adding new user (on the client side?)
 */
router.post('/addFriend', function (req, res, next) {
    verifyToken(req.body.token, (err, decodedToken) => {
        if (err) {
            console.log(error("Error - the token is either not present or invalid! Stopping request..."));
            res.status(403).send({"status": "unauthorized"}).end();
            return;
        }

        console.log(notice("An user wants to add new friend, initiating..."));
        console.log(notice("Parsing passed arguments..."));
        var userID = ObjectId(decodedToken._id);
        if (!(req.body.friendID.match(/^[0-9a-fA-F]{24}$/))) {
            console.log(error("Failed to add new friend - the expected parameters were not present or invalid!"));
            res.status(200).send({"status": "error"}).end();
        }
        else if (userID == ObjectId(req.body.friendID)) {
            console.log(warn("Failed to add new friend - one cannot add himself as a friend!"));
            res.status(200).send({"status": "himself"}).end();
        } else {
            var friendID = ObjectId(req.body.friendID);
            mongo.connect("serverdb", ["user"], function (db) {
                console.log(notice("Checking if the friend exists..."));
                db.user.find({"_id": friendID}, (err, data) => {
                    if (err) {
                        console.log(error("Failed to add friend - there was an error while connecting to the database!"));
                        res.status(200).send({"status": "error"}).end(() => {
                            db.close();
                        });
                    } else {
                        if (data.length == 0) {
                            console.log(error("Failed to add friend - the friend does not exists!"));
                            res.status(200).send({"status": "error"}).end(() => {
                                db.close();
                            });
                        } else {
                            console.log(notice("Checking if there is a friendship bond already between passed users..."));
                            db.user.find({
                                    $and: [
                                        {"_id": {$in: [userID, friendID]}},
                                        {"friendsList": {$in: [userID, friendID]}}
                                    ]
                                }, {'_id': 1}, (err, data) => {
                                    if (err) {
                                        console.log(error("Failed to add friend - there was an error while connecting to the database!"));
                                        res.status(200).send({"status": "error"}).end(() => {
                                            db.close();
                                        });
                                    } else {
                                        if (data.length != 0) {
                                            console.log(error("Failed to add friend - One of the friends does not exist or the users already have a bond!"));
                                            res.status(200).send({"status": "already"}).end(() => {
                                                db.close();
                                            });
                                        } else {
                                            console.log(notice("There was no friendship bond between users! Adding a friendship..."));
                                            var bulk = db.user.initializeUnorderedBulkOp();
                                            bulk.find({"_id": userID}).update({$addToSet: {"friendsList": friendID}});
                                            bulk.find({"_id": friendID}).update({$addToSet: {"friendsList": userID}});
                                            bulk.execute((err, data) => {
                                                if (err) {
                                                    console.log(error("Failed to add friend - there was an error while connecting to the database!"));
                                                    res.status(200).send({"status": "error"}).end(() => {
                                                        db.close();
                                                    });
                                                } else {
                                                    console.log(notice("Successfully created a friendship between " + userID + " and " + friendID));
                                                    removeFriend(userID, friendID, true, (resp) => {
                                                        res.status(200).send({"status": "ok"}).end(() => {
                                                            db.close();
                                                        });
                                                    });
                                                }
                                            });
                                        }
                                    }
                                }
                            )
                        }
                    }
                });
            });
        }
    });
});





/**
 * @params: (String) friendID
 *          It expects at least one parameter - friendID. You should pass it in request
 *          Explanation:
 *              <userID> wants to add <friendID> to his friendList, so he sends a request to <friendName>
 * @functionality: This route sends a request to specified user
 *        It first checks if the required parameters are present,
 *        Then it checks if the userID is not the same as friendID (if it is, it returns {"status" : "himself"}
 *        we now need to see if there is a request already between those users
 *        Then it checks if the bond exists already between users (it tries to locate those users, that are:
 *              - logins one of the following: [userID, friendID]
 *              - their friendList-s contain one of the following: [userID, friendID]
 *              So all routes:
 *                  * userID contains userID inside his friendList (he is his own friend - unacceptable)
 *                  * userID contains friendID inside his friendList (he already has this friend - unacceptable)
 *                  * friendID contains friendID on his friendList - same as 1st
 *                  * friendID contains userID on his friendList - same as 2nd
 *              Will be rejected
 *        Then it adds a request to the passed friendID
 *        Otherwise, in case of server error or inaccesibility, it returns {"status" : "error"}
 * @statusCodes:
 *      "error" - in case of problem with connection to database, internal problem or invalid parameters
 *      "himself" - in case the user wants to add himself as a friend
 *      "already" - in case the user already has a friendship bond with the passed friendName
 *      "nouser" -  if one of the users does not exist
 *      "ok" -    the friendship bond was successfully created between the passed users
 * @warning: if some kind of failure will happen after inserting one friend and the second query
 *              could not be completed, we will end up with inconsistent data!
 *              we would have to implement our own rollback mechanism for that
 * @todo: testing
 */
router.post('/addFriendRequest', function (req, res, next) {
    verifyToken(req.body.token, (err, decodedToken) => {
        if (err) {
            console.log(error("Error - the token is either not present or invalid! Stopping request..."));
            res.status(403).send({"status": "unauthorized"}).end();
            return;
        }

        console.log(notice("An user wants to send friend request, initiating..."));
        console.log(notice("Parsing passed arguments..."));
        var userID = ObjectId(decodedToken._id);
        var friendName = req.body.friendName;
        if (!friendName) {
            console.log(error("Failed to send friend request - the expected parameters were not present or invalid!"));
            res.status(200).send({"status": "error"}).end();
        }else if (userID == ObjectId(req.body.friendID)) {
                console.log(warn("Failed to send new friend request - one cannot add himself as a friend!"));
                res.status(200).send({"status": "himself"}).end();
        } else {
            mongo.connect("serverdb", ["user"], function (db) {
                console.log(notice("Checking if the friend exists..."));
                db.user.findOne({"login": friendName}, {"_id": 1}, (err, data) => {
                    if (err) {
                        console.log(error("Failed to send friend request - there was an error while connecting to the database!"));
                        res.status(200).send({"status": "error"}).end(() => {
                            db.close();
                        });
                    } else {
                        if (data.length == 0) {
                            console.log(error("Failed to send friend request - the friend does not exists!"));
                            res.status(200).send({"status": "nouser"}).end(() => {
                                db.close();
                            });
                        } else {
                            var friendID = ObjectId(data._id);
                            if (userID+"" == friendID+"") {
                                console.log(warn("Failed to send friend request - one cannot add himself as a friend!"));
                                res.status(200).send({"status": "himself"}).end();
                            } else {
                                console.log(notice("Checking if the users have already a pending friend request from one of each..."));
                                db.user.find(
                                    {
                                        $and: [
                                            {"_id": {$in: [userID, friendID]}},
                                            {"friendRequestsList": {$in: [userID, friendID]}}
                                        ]
                                    }, {'_id': 1}, (err, data) => {
                                        if (err) {
                                            console.log(error("Failed to add friend - there was an error while connecting to the database!"));
                                            res.status(200).send({"status": "error"}).end(() => {
                                                db.close();
                                            });
                                        } else {
                                            if (data.length != 0) {
                                                console.log(error("Failed to add friend - One of the friends does not exist or the request has already been sent!"));
                                                res.status(200).send({"status": "already"}).end(() => {
                                                    db.close();
                                                });
                                            } else {
                                                console.log(notice("Check successful! The users do not have a friend request pending between each other!"));
                                                console.log(notice("Checking if there is a friendship bond already between passed users..."));
                                                db.user.find({
                                                        $and: [
                                                            {"_id": {$in: [userID, friendID]}},
                                                            {"friendsList": {$in: [userID, friendID]}}
                                                        ]
                                                    }, {'_id': 1}, (err, data) => {
                                                        if (err) {
                                                            console.log(error("Failed to send friend request - there was an error while connecting to the database!"));
                                                            res.status(200).send({"status": "error"}).end(() => {
                                                                db.close();
                                                            });
                                                        } else {
                                                            if (data.length != 0) {
                                                                console.log(error("Failed to send friend request - One of the friends does not exist or the users already have a bond!"));
                                                                res.status(200).send({"status": "already"}).end(() => {
                                                                    db.close();
                                                                });
                                                            } else {
                                                                console.log(notice("There was no friendship bond between users! Sending a request..."));
                                                                db.user.update({"_id": friendID}, {$addToSet: {"friendRequestsList": userID}}, (err, data) => {
                                                                    if (err) {
                                                                        console.log(error("Failed to send friend request - there was an error while connecting to the database!"));
                                                                        res.status(200).send({"status": "error"}).end(() => {
                                                                            db.close();
                                                                        });
                                                                    } else {
                                                                        console.log(notice("Successfully sent a friend request from " + userID + " to " + friendID));
                                                                        res.status(200).send({"status": "ok"}).end(() => {
                                                                            db.close();
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }
                                                )
                                            }
                                        }
                                    }
                                );
                            }
                        }
                    }
                });
            });
        }
    });
});





/**
 * @params: (String) friendID
 *          (boolean) isFriendRequest - true for removing a friend request, false for removing a friend
 *      It expects at least two parameters - friendID and isFriendRequest. You should pass them in request
 *      Explanation:
 *          Case 1: isFriendRequest == false:
 *              <login> wants to delete <friendID> from his friendlist
 *          Case 2: isFriendRequest == true;
 *              <login> wants to remove a friend request <friendID> sent him
 * @functionality: This route removes a friendship between userID and friendID or removes a request between users
 *                 It does that by calling removeFriend() with the passed arguments
 *        It first checks if the required parameters are present,
 *        Then it checks if the login is not the same as friendName (if it is, it returns {"status" : "himself"}
 *        Then it calls removeFriend() function
 * @statusCodes:
 *      "error" - in case of problem with connection to database, internal problem or invalid parameters
 *      "himself" - in case the user wants to remove himself from friend list
 *      "ok" -    the friendship bond was successfully removed between the passed users
 * @warning: if some kind of failure will happen after removing one friend and the second query
 *              could not be completed, we will end up with inconsistent data!
 *              we would have to implement our own rollback mechanism for that
 * @todo: testing
 */
router.post('/removeFriend', function (req, res, next) {
    verifyToken(req.body.token, (err, decodedToken) => {
        if (err) {
            console.log(error("Error - the token is either not present or invalid! Stopping request..."));
            res.status(403).send({"status": "unauthorized"}).end();
            return;
        }

        console.log(notice("An user wants to remove friend, initiating..."));
        console.log(notice("Parsing passed arguments..."));
        var userID = decodedToken._id;
        var isFriendRequest = req.body.isFriendRequest;
        if (!(req.body.friendID.match(/^[0-9a-fA-F]{24}$/)) || isFriendRequest == null) {
            console.log(error("Failed to remove friend - the expected parameters were not present or invalid!"));
            res.status(200).send({"status": "error"}).end();
        } else {
            removeFriend(userID, req.body.friendID, isFriendRequest, (resp) => {
                res.status(200).send(resp).end();
            });
        }
    });
});


/**
 * @params: (String/ObjectId) userID
 *          (String/ObjectId) friendID
 *          (boolean) isFriendRequest - true for removing a friend request, false for removing a friend
 *          (function) callback(resp) - the function which will be called at the all ends of that function
 *                      (after the function completes). resp will be populated with response ({"status" : ???})
 *      It expects at least three parameters - userID, friendID and isFriendRequest. You should pass them in request
 *      Explanation:
 *          Case 1: isFriendRequest == false:
 *              <login> wants to delete <friendName> from his friendlist
 *          Case 2: isFriendRequest == true;
 *              <userID> wants to remove a friend request <friendID> sent him
 * @functionality: This function removes a friendship between userID and friendID or removes a request between users
 *                 It does that by removing each other in their friendList (or, in case of request, from <userID>'s)
 *        It first checks if the required parameters are present,
 *        Then it checks if the userID is not the same as friendID (if it is, it returns {"status" : "himself"}
 *        In case if isFriendRequest, we now need to see if there is a request already between those users
 *
 *        Then it removes a friendship from each user, or removes a request from the passed friendName
 * @statusCodes:
 *      "error" - in case of problem with connection to database, internal problem or invalid parameters
 *      "himself" - in case the user wants to remove himself from friend list
 *      "ok" -    the friendship bond was successfully removed between the passed users
 * @warning: if some kind of failure will happen after removing one friend and the second query
 *              could not be completed, we will end up with inconsistent data!
 *              we would have to implement our own rollback mechanism for that
 * @whyNotRoute:  because at addFriend, we would like also to delete a friend, so for an easy access to the same code
 *                a function is needed
 * @todo: testing
 */
function removeFriend(userID, friendID, isFriendRequest, callback){
    userID = ObjectId(userID);
    friendID = ObjectId(friendID);
    mongo.connect("serverdb", ["user"], function (db) {
        if(isFriendRequest){
            console.log(notice("Deleting friendship request of "+friendID+" from "+userID+" friendRequestsList..."));
            db.user.update({"_id" : userID}, {$pull: {"friendRequestsList": friendID}}, (err, data) => {
                if (err) {
                    console.log(error("Failed to remove friend request - there was an error while connecting to the database!"));
                    db.close();
                    if(callback)  callback({"status": "error"});
                } else{
                    console.log(notice("Friend request successfully deleted!"));
                    db.close();
                    if(callback)  callback({"status": "ok"});
                }
            });
        }else{
            console.log(notice("Deleting the friendship between "+friendID+" and "+userID+"..."));
            var bulk = db.user.initializeUnorderedBulkOp();
            bulk.find({ "_id": userID }).update({$pull: {"friendsList": friendID}});
            bulk.find({ "_id": friendID }).update({$pull: {"friendsList": userID}});
            bulk.execute((err, data) =>{
                if (err) {
                    console.log(error("Failed to remove the friendship - there was an error while connecting to the database!"));
                    db.close();
                    if(callback)  callback({"status": "error"});
                } else{
                    console.log(notice("Friendship successfully deleted!"));
                    db.close();
                    if(callback)  callback({"status": "ok"});
                }
            });
        }
    });
}




module.exports = router;