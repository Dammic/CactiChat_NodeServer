var mongojs = require('mongojs');
var check = require('check-types');
var clc = require('cli-color');
var error = clc.red.bold;
var warn = clc.yellow;
var notice = clc.blue;
var databaseURL = require('../config.js').databaseURL;

var db;

exports.connect = function (databaseName, collectionsArray, callback) {

    console.log(notice("Connecting to database"));
    if (check.not.string(databaseName) || check.not.array(collectionsArray))
        throw new Error("dataBase must be a String parameter and collectionList must be an array!");
    db = mongojs(databaseURL + databaseName, collectionsArray);

    db.on('error',   (err)  =>  {console.log(error(err));});
    db.on('connect', ()     =>  {console.log(notice('Database connected'));});


    if (callback) callback(db);
     else  db.close();
};

exports.disconnect = function (callback) {
    db.close();
    console.log("Disconnected from database");
    if (callback)  callback();

};

exports.ObjectId = mongojs.ObjectId;
