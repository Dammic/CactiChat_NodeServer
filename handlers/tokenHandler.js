var express = require('express');
var router = express.Router();
var path = require('path');
var mongo = require('../handlers/dbHandler.js');
var ObjectId = mongo.ObjectId;
var clc = require('cli-color');
var check = require('check-types');
var error = clc.red.bold;
var warn = clc.yellow;
var notice = clc.blue.bold;
var crypto = require('crypto');
var bcrypt = require('bcrypt-nodejs');
var secret = require('../config.js').secret;
var jwt = require('jsonwebtoken');

//@TODO: we should also store an information about phone and check if the sender has the same. It would have to be ciphered though
exports.verifyToken = function(token, callback) {
    // check header or url parameters or post parameters for token
    if (token) {
        // verifies secret and checks token
        console.log(notice("Verifying passed token..."));
        jwt.verify(token, secret, function(err, decodedToken) {
            if (err) {
                console.log(warn("Token invalid! The request needs to be stopped in the callback"));
                if(callback)  callback(err, null);
            } else {
                // if everything is good, save to request for use in other routes
                console.log(notice("Token successfully verified! Continuing with the request..."));
                if(callback)  callback(null, decodedToken);
            }
        });
    } else{
        console.log(warn("Error - User did not provide token! Stopping request..."));
        if(callback)  callback(new Error("Error - User did not provide token!"), null);
    }
};